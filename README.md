# pse_jenkins

Module for installing and configuring Jenkins.

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with pse_jenkins](#setup)
    * [What pse_jenkins affects](#what-pse_jenkins-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with pse_jenkins](#beginning-with-pse_jenkins)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

Installs Jenkins and Java dependencies as well as adding firewall ports.

## Setup

### What pse_jenkins affects

Installs specific version of Java to support Jenkins.

### Setup Requirements

No additional dependencies are required for this module.

### Beginning with pse_jenkins

```
puppet apply /etc/puppetlabs/code/environments/production/modules/pse_jenkins/examples/pse_jenkins.pp
```

## Usage

The main class has 6 parameters:
* memory_min_avail - Monitoring for the minimum amount of preferred free memory.
* memory_min_total - Prerequisite installation check for the use of Jenkins.
* jenkins_port - The HTTP port on which Jenkins is configured.
* java_version - The version of Java being installed.
* supported_os - A String Array of supported Operating Systems.
* enable_monitoring - Boolean toggle for monitoring available memory.

```
class { 'pse_jenkins':
    memory_min_avail  => 64,
    memory_min_total  => 256,
    jenkins_port      => 8000,
    java_version      => 'java-11-openjdk-devel',
    supported_os      => ['centos','redhat'],
    enable_monitoring => true,
}
```

The examples folder has different testing parameters:

```
├── examples
│   ├── pse_jenkins_all.pp
│   ├── pse_jenkins_high.pp
│   ├── pse_jenkins_portinuse.pp
│   ├── pse_jenkins.pp
│   └── pse_jenkins_unsupportedos.pp
```

There are four functions which support the operation of the module:

```
├── lib
│   ├── facter
│   │   └── jenkins_installed.rb
│   └── puppet
│       └── functions
│           └── pse_jenkins
│               ├── firewall_add_port.rb
│               ├── firewall_port_check.rb
│               └── port_check_ruby.rb
```

## Limitations

Firewall ports are added in a procedural method, not declarative.  They are not made 
permanent when added.

