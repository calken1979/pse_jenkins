# @summary Install Jenkins and dependencies
#
# Include the Jenkins repo and install the Jenkins and Java rpms.
#
# @example
#   include pse_jenkins::install
class pse_jenkins::install {

    yumrepo { 'Jenkins':
        descr    => 'Jenkins-stable',
        baseurl  => 'http://pkg.jenkins.io/redhat-stable',
        gpgcheck => 1,
        gpgkey   => 'https://pkg.jenkins.io/redhat-stable/jenkins.io.key',
        enabled  => 1,
    }

    package { $pse_jenkins::java_version:
        ensure   => 'installed',
        provider => 'yum',
    }

    package { 'jenkins':
        ensure   => 'installed',
        provider => 'yum',
        require  => [Yumrepo['Jenkins'], Package[$pse_jenkins::java_version]],
    }

}
