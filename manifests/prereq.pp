# @summary Prerequisit checks for installing Jenkins
#
# Tests for available memory, Supported OSes and port availability
#
# @example
#   include pse_jenkins::prereq
class pse_jenkins::prereq {

    $mb = (1024 * 1024 * 8)
    $memory_mb_total = $facts['memory']['system']['total_bytes'] / $mb

    # test the amount of memory available on the system
    if $memory_mb_total < $pse_jenkins::memory_min_total {
        alert ("Minimum total memory not met: ${memory_mb_total}MB available, ${pse_jenkins::memory_min_total}MB required.")
    }

    # Check to see if the OS is supported
    case $facts['os']['name'] {
        *$pse_jenkins::supported_os: { notice ("OS supported ${facts['os']['name']}")}
        default:  {
            warning ("OS not supported: ${facts['os']['name']}")
        }
    }

    # Test to see if the port is free
    if pse_jenkins::port_check_ruby($pse_jenkins::jenkins_port) {
        crit ("Port ${pse_jenkins::jenkins_port} already in use by a process other than Jenkins")
    } else {
        notice ("Port ${pse_jenkins::jenkins_port} is available to use by Jenkins")
    }

}
