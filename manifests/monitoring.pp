# @summary Monitoring of the Jenkins environment.
#
# Tests for the available memory within the system.
#
# @example
#   include pse_jenkins::monitoring
class pse_jenkins::monitoring {

    $mb = (1024 * 1024 * 8)
    $memory_mb_total = $facts['memory']['system']['total_bytes'] / $mb
    $memory_mb_used = $facts['memory']['system']['used_bytes'] / $mb

    $memory_mb_avail = $memory_mb_total - $memory_mb_used
    if $memory_mb_avail < $pse_jenkins::memory_min_avail {
        warning ("Minimum available memory level not met: ${memory_mb_avail}MB available, ${pse_jenkins::memory_min_avail}MB required.")
    }

}
