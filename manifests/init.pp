# @summary Installation of Jenkins configured to the PSE challenge
#
# The module installs the latest stable version of Jenkins along with the pre-requisites.
#
# The version of Java is limited to the supported versions via parameter.
#
# @example
#   include pse_jenkins
#
#   class { 'pse_jenkins':
#       jenkins_port => 8000,
#   }
#
# @param memory_min_avail The minimum available memory for monitoring.
# @param memory_min_total The minimum total memory for prerequisites.
# @param jenkins_port The port Jenkins will be configured on.
# @param java_version The Java version to be installed. Limited by Enumeration.
# @param supported_os Array of Supported Operating Systems.
# @param enable_monitoring Boolean to enable or disabled monitoring.
class pse_jenkins (
    Integer                                                  $memory_min_avail  = 64,
    Integer                                                  $memory_min_total  = 256,
    Integer                                                  $jenkins_port      = 8000,
    Enum['java-1.8.0-openjdk-devel','java-11-openjdk-devel'] $java_version      = 'java-11-openjdk-devel',
    Array[String]                                            $supported_os      = ['centos','redhat'],
    Boolean                                                  $enable_monitoring = true,
){

    unless $facts['jenkins_installed'] { include pse_jenkins::prereq }
    include pse_jenkins::install
    include pse_jenkins::config
    include pse_jenkins::service
    if $enable_monitoring { include pse_jenkins::monitoring }

}
