# @summary Management of the Jenkins service
#
# Ensure the Jenkins service is running and enabled
#
# @example
#   include pse_jenkins::service
class pse_jenkins::service {

    service { 'jenkins':
        ensure     => 'running',
        enable     => 'true',
        provider   => 'redhat',
        require    => Package['jenkins'],
        hasrestart => 'true',
    }

}
