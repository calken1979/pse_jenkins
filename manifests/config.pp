# @summary Update the configuration for Jenkins
#
# The class defines the /etc/sysconfig/jenkins file and adds the defined port.
# This will only occur if the port is free and not being used by another service.
# The port will be checked against the firewall and if not specified, attempted to be added.
#
# @example
#   include pse_jenkins::config
class pse_jenkins::config {

    # check if the port defined is free so the configuration can be applied or updated
    unless pse_jenkins::port_check_ruby($pse_jenkins::jenkins_port) {
        file { 'jenkins_config':
            path    => '/etc/sysconfig/jenkins',
            content => epp('pse_jenkins/sysconfig_jenkins.epp'),
            notify  => Service['jenkins'],
            require => Package['jenkins'],
        }

        # Test is port has already been added to the firewall
        unless pse_jenkins::firewall_port_check($pse_jenkins::jenkins_port) {
            warning ("Port ${pse_jenkins::jenkins_port} is not open on the firewall, attempting to add")

            # try and add the port to the firewall
            if pse_jenkins::firewall_add_port($pse_jenkins::jenkins_port) {
                notice ("Successfully added port ${pse_jenkins::jenkins_port} to the firewall")
            } else {
                warning ("Unable to add port ${pse_jenkins::jenkins_port} to the firewall")
            }
        }

    } else {
        crit ("Unable to change configuration, port ${pse_jenkins::jenkins_port} is in use")
    }

}
