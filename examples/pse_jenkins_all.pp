class { 'pse_jenkins':
    memory_min_avail  => 64,
    memory_min_total  => 256,
    jenkins_port      => 8000,
    java_version      => 'java-11-openjdk-devel',
    supported_os      => ['centos','redhat'],
    enable_monitoring => true,
}
