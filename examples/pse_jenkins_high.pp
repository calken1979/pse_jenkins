class { 'pse_jenkins':
    jenkins_port     => 8000,
    memory_min_avail => 512,
    memory_min_total => 4096,
}
