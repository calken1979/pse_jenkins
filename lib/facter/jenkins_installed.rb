# frozen_string_literal: true

# add fact jenkins_installed
Facter.add('jenkins_installed') do
  # https://puppet.com/docs/puppet/latest/fact_overview.html
  setcode do
    # test if the Jenkins RPM is installed using yum
    output = Facter::Core::Execution.execute('/bin/yum list installed | grep jenkins')
    output.nil? || output.empty? ? false : true
  end
end
