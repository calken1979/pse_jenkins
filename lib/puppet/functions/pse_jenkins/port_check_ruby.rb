# Checks to see if the provided port is being used by a process other than Jenkins
Puppet::Functions.create_function(:'pse_jenkins::port_check_ruby') do
  # Checks port as integer
  # @param port The port being checked
  # @return [Boolean] Returns true if the port is in use
  # @example Checking port 8000
  #   port_check_ruby(8000) => false
  dispatch :port_check_ruby do
    param 'Integer', :port
    return_type 'Boolean'
  end

  def port_check_ruby(port)
    # find the PID of the Jenkins process
    pid = Facter::Core::Execution.execute("ps -ef | grep jenkins.war | grep -v grep | awk '{print $2}'")
    check_pid = pid.nil? || pid.empty? ? '' : '| grep -vw ' + pid + '/java'
    # find if the defined port is currently listening on IPV4 for anything other than the Jenkins process
    output = Facter::Core::Execution.execute('/bin/netstat -tulnp | grep LISTEN | grep -w ' + port.to_s + ' | grep tcp ' + check_pid)
    output.nil? || output.empty? ? false : true
  end
end
