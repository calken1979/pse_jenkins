
# Check if port is already added to the firewall
Puppet::Functions.create_function(:'pse_jenkins::firewall_port_check') do
  # Checks port as Integer
  # @param port The port being checked
  # @return [Boolean] Returns true if the port is already added
  # @example Checking port 8000
  #   firewall_port_check(8000) => true
  dispatch :firewall_port_check do
    param 'Integer', :port
    return_type 'Boolean'
  end

  def firewall_port_check(port)
    output = Facter::Core::Execution.execute('/bin/firewall-cmd --list-ports | grep -w ' + port.to_s + '/tcp')
    output.nil? || output.empty? ? false : true
  end
end
