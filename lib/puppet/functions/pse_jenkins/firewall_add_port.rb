# adds a port to the firewall
Puppet::Functions.create_function(:'pse_jenkins::firewall_add_port') do
  # Adds port as integer
  # Port is limited to tcp and is not added permanently
  # @param port The port being added
  # @return [Boolean] Returns true if the port was added successfully
  # @example Adding port 8000
  #   firewall_add_port(8000) => true
  dispatch :firewall_add_port do
    param 'Integer', :port
    return_type 'Boolean'
  end

  def firewall_add_port(port)
    output = Facter::Core::Execution.execute('/bin/firewall-cmd --add-port=' + port.to_s + '/tcp')
    output.eql?('success') ? true : false
  end
end
